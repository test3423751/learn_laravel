<!DOCTYPE html>
<html>
    <body>
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>

        <form action = "/welcome" method = "POST">
        @csrf
        <label>First name:</label><br><br>
            <input type="text" name="first_name"name="first_name"><br><br>
        <label>Last name:</label><br><br>
            <input type="text" name="last_name"><br>

        <p>Gender:</p>
            <input type="radio" id="male" name="gender" value="MALE">
            <label for="male">Male</label><br>
            <input type="radio" id="female" name="gender" value="FEMALE">
            <label for="female">Female</label><br>
            <input type="radio" id="other" name="gender" value="OTHER">
            <label for="other">Other</label><br><br>
        
        <label>Nationality:</label><br><br>
            <select name="Nationality">
                <option value="indonesian">Indonesian</option>
                <option value="malaysian">Malaysian</option>
                <option value="australian">Australian</option>
                <option value="american">American</option>
                <option value="belgian">Belgian</option>
            </select><br><br>
        
        <label>Language Spoken:</label><br><br>
            <input type="checkbox">Bahasa Indonesia<br>
            <input type="checkbox">English<br>
            <input type="checkbox">Other<br>
        <br>

        <label>Bio:</label><br><br>
            <textarea name="message" rows="10" cols="35"></textarea><br><br>
    
        <input type="submit" name="submit" value="Sign Up">
    </form>
    </body>
</html>